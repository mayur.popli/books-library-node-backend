import express from "express";
import { Request, Response } from "express";
const router = express.Router();
import { validationResult, body } from "express-validator";
import { signUp } from "../controllers/signUp";

router.post("/signup", [
  body("username").notEmpty().withMessage("enter username"),
  body("password").notEmpty().withMessage("enter password"),
  body("email").notEmpty().withMessage("enter Email")
],signUp);


export default router
