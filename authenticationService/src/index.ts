import express from "express";
import dotenv from "dotenv";
import mongoose from "mongoose";
import logger from "./logger";
import uploader from "./config/multer";
import authenticateRouter from "./Routers/authenticateRouter";

const app = express();
app.use(uploader.single("image"));
app.use(express.json({ limit: "10mb" }));

dotenv.config({ path: "./.env" });

let port = 9000;
if (process.env.PORT) {
  port = +process.env.PORT;
}
console.log(process.env.MONGO_URL,process.env.PORT)

if (process.env.MONGO_URL) {
  mongoose
    .connect(process.env.MONGO_URL)
    .then(() => {
      logger.info("Connected to MongoDB Cloud Successfully......");
    })
    .catch((error) => {
      logger.error("error while connecting to database", error);
      process.exit(1);
    });
}

app.get("/", async (req, res) => {
  logger.info(req);
  res.status(200).send({
    message: "working",
  });
});

app.use("/user", authenticateRouter);

app.listen(port, () => {
  logger.info(`server started at ${port}`);
});
