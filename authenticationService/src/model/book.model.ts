import mongoose from "mongoose";

const BookSchema = new mongoose.Schema({
  name: { type: String, required: true },
  genre: {
    type: String,
    enum: [
      "Fiction",
      "Mystery",
      "Thriller",
      "Science-Fiction",
      "Historical-Fiction",
      "Non-Fiction",
      "Fantasy",
      "Thriller",
      "Romance",
      "Horror",
      "Biography",
      'Adventure',
      'Self-Help',
      'Comic',
      'Poetry'
    ],
    required: true
  },
  coverPic: {type: String}, // profile picture for the book
  preview: {type: String}, // pdf for viewing preview
  rating: {type:Number, enum:[1,2,3,4,5,6,7,8,9,10]}, //ratings given by users
});

