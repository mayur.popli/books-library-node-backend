import { createLogger, transports, format } from "winston";

const logger = createLogger({
  format: format.printf((info) => {
    return `${info.level} - ${info.message}`;
  }),
  level: "debug",
  transports: [new transports.Console({ level: "silly" })],
});

export default logger

// import { createLogger, transports, format } from "winston";

// const logger = createLogger({
//     format: format.printf((info) => {
//       return `${info.level} - ${info.message}`;
//     }),
//     level: "debug",
//     transports: [new transports.Console({ level: "silly" })],
//   });

//   export default logger;
