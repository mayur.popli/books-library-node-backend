import { Request, Response } from "express";
import { User } from "../model/user.model";
import { validationResult } from "express-validator";
import bcrypt from "bcrypt";

export const signUp = async (req: Request, res: Response) => {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { username, password, email } = req.body;
    const alreadyExist = await User.findOne({
      $or: [{ username: username }, { email: email }],
    });
    console.log(
      "🚀 ~ file: signUp.ts:15 ~ signUp ~ alreadyExist:",
      alreadyExist
    );
    if (alreadyExist) {
      return res
        .status(400)
        .json({ errors: [{ msg: "Username already exist" }] });
    }
    const salt = await bcrypt.genSalt(10);
    const hashedPass = await bcrypt.hash(password, 10);
    const newUser = new User({ username, password: hashedPass, email });
    newUser.save();
    res.send({
      success: true,
      response: "ACK",
      message: "successfully signed In",
    });
  } catch (error) {
    res.send({ success: false, message: "there is an error" });
  }
};
